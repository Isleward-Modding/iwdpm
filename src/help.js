define([
	//
], function(
	//
) {
	return [
		"Isleward Package Manager [IWDPM]",
		"A \"package\" manager for managing mods for Isleward, the multiplayer, moddable, extensible roguelike built with NodeJS, JS, HTML and CSS.",
		"",
		"Usage:",
		//"    iwdpm command usage",
		"    iwdpm [(help | h)]",
		"    iwdpm (information | info) <mod>"//,
		//"    iwdpm (install | i)"
	];
});