define([
	'src/modlist' // temporary
], function(
	modlist
) {
	return function(name) {
		var mod = modlist.find((mod) => {
			return mod.name == name;
		});
		
		if(mod) {
			return mod;
		} else {
			// aliases for mods
			return modlist.find((mod) => {
				return mod.aliases.includes(name);
			})
		}
	}
});
