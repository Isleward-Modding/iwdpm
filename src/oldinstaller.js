(async function () {

// setup
const source = 'https://gitlab.com/snippets/1684569/raw'
const rp = require('request-promise');
const c = require('async-child-process');
exec = c.execAsync;
const fs = require('fs');

// helpers
function err(str) {
	console.error("ERROR: " + str);
	process.exit(1);
}

// parse args
args = process.argv;
// get rid of node
if(args[0].endsWith("node.exe")) {
	args = args.slice(1);
}
// get rid of this file
args = args.slice(1);
mod = args[0];

// fetch references
data = await rp(source);
data = JSON.parse(data);
console.log('done fetching sources')

// check for missing mod
if(data[mod] == undefined) {
	err("mod source for '" + mod + "' not found");
}

// get mod url
url = data[mod];

// status messages
console.log("preparing to download '" + mod + "'")
console.log("cloning mod from '" + url + "'")

// clone mod
try {
	r = await exec('git clone ' + url + ' src/server/mods/' + mod);
	//console.log(r.stdout);
} catch(e) {
	switch(e.code) {
	case 128:
		// in the future, try to update the mod
		err("mod folder already exists, or mod source references a repository that does not exist");
		break;
	default:
		err("unexpected error");
		break;
	}
}

// status message
console.log("installed " + mod + " to mods folder successfully!");

})();