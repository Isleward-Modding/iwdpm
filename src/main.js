var requirejs = require('requirejs');

var path = require('path');

requirejs.config({
	baseUrl: path.join(__dirname, '..'),

	nodeRequire: require
});

requirejs([
	'minimist',

	'src/logger',
	'src/install',
	'src/information',
	'src/help'
], function(
	minimist,
	
	logger,
	install,
	information,
	helpText
) {
	logger.log('IWDPM started', 'info');
	
	// arguments
	var args = minimist(process.argv.slice(2));
	
	var command = args._.shift();
	
	switch(command) {
		//case "install":
		//case "i":
		//	logger.write(JSON.stringify(install(args._[0])));
		//	break;
		case "information":
		case "info":
			if(!args._[0]) {
				logger.warn('Mod name to check is required.', 'args')
				return;
			}
		
			var result = information(args._[0]);
		
			logger.log(result.join('\n      '), 'mod');
			break;
		default:
			// if not found, extra message and show text
			logger.warn('Command not found. Showing help text: ');
		case "help":
		case "h":
			logger.log(helpText.join('\n       '), 'help');
			break;
	}
	
	logger.log('IWDPM done', 'info');
	process.exit(0);
});