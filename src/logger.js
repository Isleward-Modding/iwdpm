define([
	//
], function(
	//
) {
	return {
		log: function(text, type = "info") {
			this.write("[" + type.toUpperCase() + "] " + text);
		},
		warn: function(warning, type = "warn") {
			var label = "[" + type.toUpperCase() + "] ";
			if(type == "warn") {
				label = "";
			}
			this.log(label + warning, "warn");
		},
		error: function(error = "An unexpected error ocurred.", type = "error") {
			this.log("[" + type.toUpperCase() + "] " + error, "error");
			process.exit(1);
		},
		
		write: function(text) {
			// log stuff to a file eventually?
			console.log(text);
		}
	}
})