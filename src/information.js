define([
	'src/getMod'
], function(
	getMod
) {
	return function(modName) {
		var mod = getMod(modName);
		
		return [
			mod.name,
			"",
			mod.description,
			"",
			"Aliases: " + mod.aliases.join(", "),
			"Git URL: " + mod.git
		];
	}
});
