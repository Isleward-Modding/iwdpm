define([
	//
], function(
	//
) {
	return [
		{
			"name": "class-necromancer",
			"description": "A basic Isleward mod that adds a new necromancer class.",
			"git": "https://gitlab.com/Isleward/ModNecromancer.git",
			"aliases": ["necromancer", "necro", "mod-necro", "class-necro", "mod-necromancer"]
		}
	]
});
