module.exports = async function (transport, source=transport.config.source) {
	var data = await transport.libs.requestPromise(transport.config.source);
	data = JSON.parse(data);
	transport.log("fetched sources/references from '" + transport.config.source + "'");
	return data;
}